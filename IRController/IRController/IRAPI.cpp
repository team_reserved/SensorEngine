#include "stdafx.h"
#include "IRAPI.h"

BOOL	state = 0x1;
Buttons	PreviousCommand = (Buttons)0;
ULONG	PreviousAmount = 0;
DWORD	PreviousCommandTick = GetTickCount();

void changeState()
{
	state ^= 0x1;
}

DWORD WINAPI cursorClick(LPVOID)
{
	if (state)
	{
		system("nircmdc sendmouse left click");
	}
	else
	{
		system("nircmdc sendkey enter press");
	}

	return 0;
}

/*
	Assistant function to centralize how acceleration
	is counted.
*/
int countAcceleration(ULONG Previous)
{
	ULONG temp = (ULONG)pow(PreviousAmount, 2) / 2;
	if (temp > 50)
		temp = 50;

	return temp + 10; // min 10, max 60
}

DWORD WINAPI cursorUp(LPVOID)
{
	if (state)
	{
		char message[30];
		sprintf_s(message, "nircmdc movecursor 0 -%d", countAcceleration(PreviousAmount));

		system(message);
	}
	else
	{
		system("nircmdc sendkey up press");
	}

	return 0;
}

DWORD WINAPI cursorDown(LPVOID)
{
	if (state)
	{
		char message[30];
		sprintf_s(message, "nircmdc movecursor 0 %d", countAcceleration(PreviousAmount));

		system(message);
	}
	else
	{
		system("nircmdc sendkey down press");
	}

	return 0;
}

DWORD WINAPI cursorRight(LPVOID)
{
	if (state)
	{
		char message[30];
		sprintf_s(message, "nircmdc movecursor %d 0", countAcceleration(PreviousAmount));

		system(message);
	}
	else
	{
		system("nircmdc sendkey right press");
	}

	return 0;
}

DWORD WINAPI cursorLeft(LPVOID)
{
	if (state)
	{
		char message[30];
		sprintf_s(message, "nircmdc movecursor -%d 0", countAcceleration(PreviousAmount));

		system(message);
	}
	else
	{
		system("nircmdc sendkey left press");
	}

	return 0;
}

DWORD WINAPI monitorStandby(LPVOID)
{
	system("nircmdc monitor async_off");

	return 0;
}

void hibernate()
{
	system("nircmdc hibernate force");
}

DWORD WINAPI increaseBrightness(LPVOID)
{
	system("nircmdc changebrightness 5");

	return 0;
}

DWORD WINAPI decreaseBrightness(LPVOID)
{
	system("nircmdc changebrightness -5");

	return 0;
}

DWORD WINAPI increaseVolume(LPVOID)
{
	system("nircmdc changesysvolume 3000");

	return 0;
}

DWORD WINAPI decreaseVolume(LPVOID)
{
	system("nircmdc changesysvolume -3000");

	return 0;
}

DWORD WINAPI muteVolume(LPVOID)
{
	//system("nircmdc mutesysvolume 2");
	system("UYIC.exe");

	return 0;
}

DWORD WINAPI buttonPlayPause(LPVOID)
{
	system("nircmdc sendkey 0xB3 press");

	return 0;
}

DWORD WINAPI buttonForward(LPVOID)
{
	system("nircmdc sendkey 0xB0 press");

	return 0;
}

DWORD WINAPI thread(LPVOID)
{
	system("nircmdc sendkey 0xB1 press");

	return 0;
}

DWORD WINAPI buttonBackward(LPVOID)
{
	system("nircmdc sendkey 0xB1 press");

	return 0;
}

DWORD WINAPI buttonAltTab(LPVOID)
{
	system("nircmdc sendkeypress alt+tab");

	return 0;
}