#pragma once

enum Buttons
{
	VolumeDown		= 0xFF22DD,
	VolumeUp		= 0xFFC23D,
	VolumeMute		= 0xFF02FD,
	BrightnessUp	= 0xFF629D,
	BrightnessDown	= 0xFFA857,
	BtnHibernate	= 0xFF42BD,
	BtnStandby		= 0xFF52AD,
	BtnRight		= 0xFF7A85,
	BtnLeft			= 0xFF30CF,
	BtnUp			= 0xFF9867,
	BtnDown			= 0xFF38C7,
	BtnOK			= 0xFF18E7,
	BtnChangeState	= 0xFF5AA5,
	BtnForward		= 0xFFB04F,
	BtnBackward		= 0xFF6897,
	BtnPlayPause	= 0xFF4AB5,
	BtnAltTab		= 0xFF10EF,
	RepeaterSignal	= 0x7FFFFFFF
};

/*enum Buttons
{
	VolumeDown		= 0x10011,
	VolumeUp		= 0x10010,
	VolumeMute		= 0x1000D,
	BrightnessUp	= 0x1004C,
	BrightnessDown	= 0x1004D,
	BtnHibernate	= 0x1000C,
	BtnStandby		= 0x1006D,
	BtnRight		= 0x1005B,
	BtnLeft			= 0x1005A,
	BtnUp			= 0x10058,
	BtnDown			= 0x10059,
	BtnOK			= 0x1005C,
	BtnChangeState	= 0x10000,
	BtnForward		= 0x10003,
	BtnBackward		= 0x10001,
	BtnPlayPause	= 0x10007,
	BtnAltTab		= 0x10009
};*/

extern Buttons	PreviousCommand;		// Used to know which command I pressed before
extern ULONG	PreviousAmount;			// Helps to calculate emulated mouse acceleration
extern DWORD	PreviousCommandTick;	// Reset PreviousAmount based on passed ticks

//	|								|
//	| --- Non-thread functions: --- |
//	|								|

/*
	Toggle between mouse and keyboard mode

	0 = keyboard
	1 = mouse
*/
void changeState();
void hibernate();

// |											|
// | ---  Asynchronous-compatible functions --- |
// |											|

/*
	Depending on the state it will either
	� Click once
	� Send Enter
*/
DWORD WINAPI cursorClick(LPVOID);

/*
	Depending on state it will either:
	� Move the mouse by [10;60] px in the upper direction
	� Send up button
*/
DWORD WINAPI cursorUp(LPVOID);

/*
	Depending on state it will either:
	� Move the mouse by [10;60] px in the down direction
	� Send down button
*/
DWORD WINAPI cursorDown(LPVOID);

/*
	Depending on state it will either:
	� Move the mouse by [10;60] px in the right direction
	� Send right button
*/
DWORD WINAPI cursorRight(LPVOID);

/*
	Depending on state it will either:
	� Move the mouse by [10;60] px in the left direction
	� Send left button
*/
DWORD WINAPI cursorLeft(LPVOID);

/*
	This command only works in systems that support this feature.
	Tries to turn off the display.
*/
DWORD WINAPI monitorStandby(LPVOID);

/*
	Obviously increases Brightness when it's available.
*/
DWORD WINAPI increaseBrightness(LPVOID);

/*
	Obviously decreases Brightness when it's available.
*/
DWORD WINAPI decreaseBrightness(LPVOID);

/*
	I'm bored to properly documentate this... It's obvious
*/
DWORD WINAPI increaseVolume(LPVOID);

/*
	Do you really need these comments?
*/
DWORD WINAPI decreaseVolume(LPVOID);

/*
	I'm totally unsure what this does.
*/
DWORD WINAPI muteVolume(LPVOID);

/*
	Press Multimedia Play/Pause button.
*/
DWORD WINAPI buttonPlayPause(LPVOID);

/*
	Press Multimedia Fast-forward button;
*/
DWORD WINAPI buttonForward(LPVOID);

/*
	Press Multimedia backward button.
*/
DWORD WINAPI buttonBackward(LPVOID);

/*
	Simoultaneosly emulate ALT+TAB once
*/
DWORD WINAPI buttonAltTab(LPVOID);