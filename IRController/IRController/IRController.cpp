#include "stdafx.h"
#include "SerialController.h"
#include <Shlwapi.h>
#include <Psapi.h>

#pragma comment(lib, "shlwapi.lib")

void registerAsAutostart()
{
	HKEY hkey = NULL;
	WCHAR Path[MAX_PATH] = { 0 };

	GetModuleFileName(NULL, Path, MAX_PATH);
	
	LSTATUS a = RegCreateKey(HKEY_CURRENT_USER, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", &hkey);
	LSTATUS b = RegSetValueEx(hkey, L"IR Computer Station", 0, REG_SZ, (BYTE*)Path, (DWORD)((wcslen(Path) + 1) * 2));
}

TCHAR* GetThisPath(TCHAR* dest, DWORD destSize)
{
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);
	return dest;
}

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	EmptyWorkingSet(GetCurrentProcess());

#ifdef _DEBUG
	AllocConsole();
	freopen("CONOUT$", "w", stdout);
	SetConsoleTitleA("IRCommander v0.1 ~ Unc3nZureD");
#else
	WCHAR dir[MAX_PATH] = { 0 };
	GetThisPath(dir, MAX_PATH);
	SetCurrentDirectory(dir);

	registerAsAutostart();
#endif
	
	CreateThread(0, 0, SerialThread, 0, 0, 0);

	Sleep(10000);
	EmptyWorkingSet(GetCurrentProcess());

	// Add Tray stuffs here
	while (true)
	{
		Sleep(120000);
	}

    return 0;
}