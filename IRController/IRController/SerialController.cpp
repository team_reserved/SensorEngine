#include "stdafx.h"
#include "SerialController.h"
#include "IRAPI.h"

#define SizeOfBuffer 2
HANDLE m_hParallelComm = 0;

void cleanExit()
{
	CloseHandle(m_hParallelComm);
}

void seekForOpenPort()
{
	char portName[] = "COM1";

	printf("Scanning for attached devices...\n");

	while (1)
	{
		for (char i = '1'; i <= '9'; i++)
		{
			portName[3] = i;

			m_hParallelComm = CreateFileA(portName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if (m_hParallelComm == INVALID_HANDLE_VALUE)
				Sleep(1000);
			else
				goto Succeed;
		}
	}

Succeed:
	printf("Device successfully connected at port %s\n", portName);

	DCB dcbSerialParams = { 0 };

	dcbSerialParams.BaudRate = CBR_9600;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = ONESTOPBIT;
	dcbSerialParams.Parity = NOPARITY;
	dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;

	if (SetCommState(m_hParallelComm, &dcbSerialParams))
	{
		printf("Device is successfully initialized!\n");
		PurgeComm(m_hParallelComm, PURGE_RXCLEAR | PURGE_TXCLEAR);
		Sleep(2000);
		printf("Ready to use!\n");
	}
	else
		printf("Device failed to initialize!\n");
}

void checkIDEs()
{

}

DWORD WINAPI SerialThread(LPVOID)
{
	if(m_hParallelComm)
		return 0;

	// Open COM port
	seekForOpenPort();

	// Close Handle on exit
	atexit(cleanExit);

	COMSTAT status;
	DWORD errors;
	BOOL retRead;
	CHAR Buffer[SizeOfBuffer];
	ZeroMemory(Buffer, SizeOfBuffer);
	DWORD dwNumberOfBytesRead, dwCommand;
	string CurrentCommand;

	// Main Loop
	while (1)
	{
		ClearCommError(m_hParallelComm, &errors, &status);


		// Read from COM port
		retRead = ReadFile(m_hParallelComm, (LPVOID)Buffer, SizeOfBuffer - 1, &dwNumberOfBytesRead, NULL);

		if (!retRead)					// if device is detached, the re-scan for open port
		{								// and restart Reading procedure
			CloseHandle(m_hParallelComm);
			printf("Device disconnected from previous port!\n");
			seekForOpenPort();
			continue;
		}

		// only accept alphabets, letters and \r\n chars
		if (Buffer[0] < '0' || Buffer[0] > 'z')
		{
			if (!(Buffer[0] == '\r' || Buffer[0] == '\n'))
			{
				continue;
			}
		}

#ifdef DebugPrintOnly
		printf("%c", Buffer[0]);
		continue;
#endif

		// If I could successfully read a byte, then append it
		if (Buffer[0])
			CurrentCommand.append(Buffer);

		// And reset the buffer
		ZeroMemory(Buffer, 2);

		// \r\n means the end of one instruction
		if (CurrentCommand.rfind("\r\n") == string::npos)
			continue;


		// Convert to numbers
		dwCommand = strtol(CurrentCommand.c_str(), 0, 16);

#ifdef Special10k
		dwCommand |= 0x10000;		// My Remote sends a special 0x10000 with some of my packets - so I added this header for each packet
#endif

		CurrentCommand.clear();

		if (dwCommand == RepeaterSignal)
			dwCommand = PreviousCommand;

		printf("0x%X\n", dwCommand);

		// Count pressing times, but exclude StateChange & Play_Pause
		if (PreviousCommand == dwCommand)
		{
			if (dwCommand == BtnUp || dwCommand == BtnDown || dwCommand == BtnLeft || dwCommand == BtnRight)
			{
				if (GetTickCount() - PreviousCommandTick < 300)
					PreviousAmount++;
				else
					PreviousAmount = 0;
			}
			else if (dwCommand == BtnPlayPause)
			{
				if (GetTickCount() - PreviousCommandTick < 1000)
					continue;
			}
			else if (dwCommand == BtnAltTab)
			{
				if (GetTickCount() - PreviousCommandTick < 800)
					continue;
			}
		}
		else
		{
			PreviousAmount = 0;
		}

		// Execute command Asynchronously
		switch (dwCommand)
		{
		case VolumeDown:
			Async(decreaseVolume);
			break;
		case VolumeUp:
			Async(increaseVolume);
			break;
		case VolumeMute:
			Async(muteVolume);
			break;
		case BrightnessUp:
			Async(increaseBrightness);
			break;
		case BrightnessDown:
			Async(decreaseBrightness);;
			break;
		case BtnHibernate:
			hibernate();
			break;
		case BtnStandby:
			Async(monitorStandby);
			break;
		case BtnRight:
			Async(cursorRight);
			break;
		case BtnLeft:
			Async(cursorLeft);
			break;
		case BtnUp:
			Async(cursorUp);
			break;
		case BtnDown:
			Async(cursorDown);
			break;
		case BtnOK:
			Async(cursorClick);
			break;
		//case BtnChangeState:
		//	changeState();
		//	break;
		case BtnForward:
			Async(buttonForward);
			break;
		case BtnBackward:
			Async(buttonBackward);
			break;
		case BtnPlayPause:
			Async(buttonPlayPause);
			break;
		case BtnAltTab:
			Async(buttonAltTab);
			break;
		}

		// Save it as previous
		PreviousCommand = (Buttons)dwCommand;
		PreviousCommandTick = GetTickCount();
	}

	return 0;
}