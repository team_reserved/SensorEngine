#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <iostream>

using namespace std;

#undef Special10k
#undef DebugPrintOnly

#ifdef _DEBUG
	#define Async(param) param(0)
#else
	#define printf
	#define printf_s

	#define Async(param) CreateThread(0, 0, param, 0, 0, 0)
	#define system(param) WinExec(param, SW_HIDE);
#endif